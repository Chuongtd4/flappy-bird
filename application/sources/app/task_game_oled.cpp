/**************************flappy bird******************************/

#include "task_game_oled.h"
//#include "bitmap.h"

#include "fsm.h"
#include "port.h"
#include "message.h"

#include "sys_ctrl.h"
#include "sys_dbg.h"

#include "view_render.h"

#include "app.h"
#include "app_dbg.h"

#include "task_list.h"

const unsigned char PROGMEM pig [] =
{
	0xff, 0xff, 0xff, 0xdf, 0x87, 0x07, 0x03, 0x03, 0x83, 0x01, 0x01, 0x01, 0xe1, 0xc1, 0xc1, 0xc1,
	0xe1, 0x01, 0xe1, 0xe1, 0xe1, 0xf3, 0xff, 0xff, 0xff, 0xff, 0xff, 0x3f, 0x0f, 0x03, 0x30, 0x10,
	0x04, 0xe7, 0xe3, 0xe2, 0xe7, 0xc3, 0x8a, 0x8b, 0x0a, 0x05, 0x4f, 0x7f, 0xbf, 0xbf, 0x7f, 0xff,
	0xff, 0xff, 0xff, 0xfe, 0xfc, 0xfc, 0xf0, 0x02, 0x02, 0xf2, 0xf6, 0xf7, 0x73, 0x03, 0x01, 0x9d,
	0x3d, 0x3d, 0xfd, 0xfc, 0xfe, 0xfd, 0xfe, 0xff, 0x3f, 0x3f, 0x3f, 0x23, 0x21, 0x20, 0x22, 0x20,
	0x2d, 0x3c, 0x3e, 0x3e, 0x3e, 0x3e, 0x3d, 0x31, 0x23, 0x20, 0x23, 0x23, 0x23, 0x27, 0x27, 0x3f,
};
const unsigned char PROGMEM logo1 [] =
{
	0x00, 0x00, 0x00, 0x20, 0x78, 0xF8, 0xFC, 0xFC, 0x7C, 0xFE, 0xFE, 0xFE, 0x1E, 0x3E, 0x3E, 0x3E,
	0x1E, 0xFE, 0x1E, 0x1E, 0x1E, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xF0, 0xFC, 0xCF, 0xEF,
	0xFB, 0x18, 0x1C, 0x1D, 0x18, 0x3C, 0x75, 0x74, 0xF5, 0xFA, 0xB0, 0x80, 0x40, 0x40, 0x80, 0x00,
	0x00, 0x00, 0x00, 0x01, 0x03, 0x03, 0x0F, 0xFD, 0xFD, 0x0D, 0x09, 0x08, 0x8C, 0xFC, 0xFE, 0x62,
	0xC2, 0xC2, 0x02, 0x03, 0x01, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x1C, 0x1E, 0x1F, 0x1D, 0x1F,
	0x12, 0x03, 0x01, 0x01, 0x01, 0x01, 0x02, 0x0E, 0x1C, 0x1F,
};

const uint8_t logo[] PROGMEM = {
	0x07, 0x00, 0x44, 0x02, 0x20, 0x0a, 0x06, 0x73, 0x4d, 0x66, 0x36, 0x33, 0x59, 0x67, 0x30, 0x28,
	0x02, 0x20, 0x11, 0x00, 0x70, 0x7f
};
const uint8_t flappybird_frame_1[] PROGMEM = {
	0x30, 0x58, 0x5E, 0xFB, 0x81, 0x85, 0x9B, 0x91, 0x91, 0x7D, 0x71, 0x7E, 0x60, 0x60, 0x60,
};
const uint8_t flappybird_frame_2[] PROGMEM = {
	0x30, 0x58, 0x5E, 0xFB, 0x81, 0x85, 0x9B, 0x91, 0x91, 0x7D, 0x71, 0x7E, 0x60, 0x60, 0x60,
};

const uint8_t  barbottom[] PROGMEM = { 0xFF, 0xFF, 0xFF, 0x42, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E };
const uint8_t  bartop[] PROGMEM = { 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x7E, 0x42, 0xFF, 0xFF, 0xFF };
const uint8_t brr[] PROGMEM = {
	0x07, 0x00, 0x44, 0x02, 0x20, 0x0a, 0x06, 0x73, 0x4d, 0x66, 0x36, 0x33, 0x59, 0x67, 0x30, 0x28,
	0x02, 0x20, 0x11, 0x00, 0x70, 0x7f
};

struct player{
	char*	type;
	int		state;
	int		level;
	int		score;
	int		x;
	int		y;
	int		jupm;

	int16_t		weigh=13;
	int16_t		higt=13;

	float speed=1;
};
player player1;
player show_demo;
player bar_top_1;
player bar_bottom_1;
player bar_top_2;
player bar_bottom_2;
player line1;
player line2;

void game_run();
void display_game_stop();
extern void display_game_over();
void check_state();
void display_realy();
void game_init();

void task_game(ak_msg_t* msg) {
	switch (msg->sig) {
	case AC_GAME_STATE:
	{	LOGIN_PRINT("AC_GAME_STATE\n");
		if(player1.state<=STOP)
		player1.state=player1.state+1;
		else player1.state=INIT;
		LOGIN_PRINT("AC_GAME_STATE: %d",player1.state);
		task_post_pure_msg(AC_TASK_GAME_ID,player1.state);
	}	break;
	case AC_GAME_INIT:
	{	LOGIN_PRINT("AC_GAME_INIT\n");
		/* reset watchdog */
		sys_ctrl_independent_watchdog_reset();
		sys_ctrl_soft_watchdog_reset();
		game_init();
	}	break;
	case AC_GAME_REALY:
	{	LOGIN_PRINT("AC_GAME_DOWN\n");
		display_realy();
	}	break;
	case AC_GAME_RUN:
	{	LOGIN_PRINT("AC_GAME_RUN\n");
		game_run();
		task_post_pure_msg(AC_TASK_GAME_ID,player1.state);
	}	break;
	case AC_GAME_STOP:
	{	LOGIN_PRINT("AC_GAME_STOP\n");
		if(player1.state==RUN)
		{
		player1.state=STOP;
		display_game_stop();
		}
	}	break;
	case AC_GAME_OVER:
	{	LOGIN_PRINT("AC_GAME_OVER\n");
		display_game_over();
	}	break;
	case AC_GAME_DOWN:
	{	LOGIN_PRINT("AC_GAME_DOWN\n");
		if(player1.state==REALY || player1.state==STOP){
			player1.state=RUN;
			task_post_pure_msg(AC_TASK_GAME_ID,player1.state);
		}
		player1.y=player1.y-4;
		player1.jupm=DOWN;
		if(player1.y<=PLAYER_MIN)player1.y=PLAYER_MIN;
	}	break;
	default:
		break;
	}
}

void game_int(){
	player1.state=INIT;
}

void game_init(){
	view_render.clear();

	view_render.setTextSize(2);
	view_render.setTextColor(WHITE,BLACK);
	view_render.setCursor(0, 0);
	view_render.print("MICRO GAME\n");

	player1.x=30;
	player1.y=PLAYER_MAX;

	player1.score=0;
	player1.level=1;
	player1.speed=0;
	player1.state=INIT;

	bar_top_1.y=30;
	bar_top_2.y=30;

	line1.y=62;//const
	line1.x=0;

	show_demo.x=30;
	show_demo.y=20;

	bar_top_1.x=-6;
	bar_top_2.x=-6;

	show_demo.weigh=128;
	show_demo.higt=64;
	view_render.drawBitmap(0, 15,brr,show_demo.weigh,show_demo.higt, 1);

	view_render.update();
}
void display_realy(){
	view_render.clear();
	/************bar_top_1***************/
	view_render.drawBitmap(bar_top_1.x, bar_top_1.y-30, bartop, 8, 20, 1);
	view_render.drawBitmap(bar_bottom_1.x, bar_bottom_1.y, barbottom, 8, 20, 1);

	/************bar_top_2***************/
	view_render.drawBitmap(bar_top_2.x, bar_top_2.y-30, bartop, 8, 20, 1);
	view_render.drawBitmap(bar_bottom_2.x, bar_bottom_2.y, barbottom, 8, 20, 1);

	/************clear***************/
	for(int i=0;i < 10;i++){
		view_render.drawFastHLine(0,i,128,0);
	}
	/************score***************/
	view_render.setTextSize(1);
	view_render.setTextColor(WHITE,BLACK);
	view_render.setCursor(0, 0);
	view_render.print("level:");
	view_render.print(player1.level);

	view_render.setTextSize(1);
	view_render.setTextColor(WHITE,BLACK);
	view_render.print("\t Score: ");
	view_render.print(player1.score++);
	/************line***************/
	line1.x=line1.x-player1.speed;
	if(line1.x<=-100)line1.x=0;
	view_render.drawBitmap(line1.x, line1.y, brr,228, 13, 1);
	/************line2***************/
	line2.x=line2.x-(player1.speed*0.1);
	if(line1.x<=-100)line1.x=0;
	view_render.drawBitmap(line1.x,10, brr,220, 2, 1);
	view_render.drawBitmap(player1.x, player1.y,flappybird_frame_1, player1.weigh, player1.higt, 1);
	view_render.update();
}

void game_run(){
	view_render.clear();

	/************player1***************/

	player1.weigh=16;
	player1.higt=12;
	if(player1.jupm==UP)
		view_render.drawBitmap(player1.x, player1.y,flappybird_frame_1, player1.weigh, player1.higt, 1);
	else
		view_render.drawBitmap(player1.x, player1.y,flappybird_frame_2, player1.weigh, player1.higt, 1);

	player1.y=player1.y+2;
	player1.jupm=UP;

	if(player1.y>=PLAYER_MAX)player1.y=PLAYER_MAX;

	/************bar_top_1***************/
	view_render.drawBitmap(bar_top_1.x, bar_top_1.y-30, bartop, 8, 20, 1);
	view_render.drawBitmap(bar_bottom_1.x, bar_bottom_1.y, barbottom, 8, 20, 1);

	bar_top_1.x =bar_top_1.x-player1.speed;
	bar_bottom_1.x=bar_top_1.x;

	if (bar_top_1.x < -10) {
		bar_top_1.x = 128;
		bar_top_1.y=random(25,45);
		bar_bottom_1.y=bar_top_1.y+random(15,30);
	}
	/************bar_top_2***************/
	view_render.drawBitmap(bar_top_2.x, bar_top_2.y-30, bartop, 8, 20, 1);
	view_render.drawBitmap(bar_bottom_2.x, bar_bottom_2.y, barbottom, 8, 20, 1);

	bar_top_2.x =bar_top_2.x-player1.speed;
	bar_bottom_2.x=bar_top_2.x;

	if (bar_top_2.x < -20){
		bar_top_2.x = random(135,200);
		bar_top_2.y=random(30,45);
		bar_bottom_2.y=bar_top_2.y+random(15,30);
	}
	/************clear***************/
	for(int i=0;i < 10;i++){
		view_render.drawFastHLine(0,i,128,0);
	}
	/************score***************/
	view_render.setTextSize(1);
	view_render.setTextColor(WHITE,BLACK);
	view_render.setCursor(0, 0);
	view_render.print("level:");
	view_render.print(player1.level);

	view_render.setTextSize(1);
	view_render.setTextColor(WHITE,BLACK);
	view_render.print("\t Score: ");
	view_render.print(player1.score++);
	/************line***************/
	line1.x=line1.x-player1.speed;
	if(line1.x<=-100)line1.x=0;
	view_render.drawBitmap(line1.x, line1.y, brr,228, 13, 1);
	/************line2***************/
	line2.x=line2.x-(player1.speed*0.1);
	if(line1.x<=-100)line1.x=0;
	view_render.drawBitmap(line1.x,10, brr,220, 2, 1);
	/************app***************/
	player1.speed=player1.speed+(player1.score/2);
	if(player1.speed>=5)player1.speed=5;

	/************check_state************/
	check_state();
	view_render.update();
}
void display_game_stop(){

	view_render.setTextSize(2);
	view_render.setTextColor(WHITE,BLACK);
	view_render.setCursor(0, 30);
	view_render.print("___STOP___");

	view_render.update();
}
void display_game_over(){
	view_render.clear();
	view_render.setTextSize(2);
	view_render.setTextColor(WHITE,BLACK);
	view_render.setCursor(0, 0);
	view_render.print(" GAME OVER");
	view_render.setTextSize(2);
	view_render.setTextColor(WHITE,BLACK);
	view_render.print("\nlevel:");
	view_render.print(player1.level);
	view_render.print("\nscore:");
	view_render.print(player1.score);
	view_render.update();
}
void check_state()
{
	if( (player1.x>(bar_top_1.x-4)) && (player1.x<(bar_top_1.x+4)) )
	{
		if( (bar_top_1.y+10<player1.y) )
			player1.state=OVER;
		else if( (bar_bottom_1.y>player1.y+25) )
			player1.state=OVER;
	}
	if( (player1.x>(bar_top_2.x-4)) && (player1.x<(bar_top_2.x+4)) )
	{
		if(bar_top_2.y+10<player1.y)
			player1.state=OVER;
		else if( (bar_bottom_2.y>player1.y+25) )
			player1.state=OVER;
	}
	if(player1.state==OVER)
		task_post_pure_msg(AC_TASK_GAME_ID,player1.state);
}
