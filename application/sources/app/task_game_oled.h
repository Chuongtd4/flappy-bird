#ifndef TASK_GAME_OLED_H
#define TASK_GAME_OLED_H


//state game//
#define MIN_STATE 0

#define INIT	1
#define REALY	2
#define RUN		3
#define STOP	4
#define OVER	5

#define MAX_STATE 6

#define DOWN 0
#define UP	 1

//weight hight
#define ROW_MAX 128
#define ROW_MIN 0
#define COLLUM_MAX 64
#define COLLUM_MIN 0

//player max,min

#define PLAYER_MAX 50
#define PLAYER_MIN 0

void display_game_init();
void display_game_run();

class Task_game_oled
{
public:
	Task_game_oled();
};


#endif // TASK_GAME_OLED_H
